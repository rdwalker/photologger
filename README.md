[![pipeline](https://gitlab.com/insect1285/photologger/badges/master/pipeline.svg)](https://gitlab.com/insect1285/photologger/commits/master)
[![coverage](https://gitlab.com/insect1285/photologger/badges/master/coverage.svg)](https://gitlab.com/insect1285/photologger/commits/master)

## Introduction
The Photologger Application is a tool to expedite the process of creating a photographic log for 
reporting.  By using this application you will get a consistent report every time without the need
to manually place photographs and text.  Users can also export a zip which includes the data
and photographs for archive or to re-upload to continue work at a later time.

Please visit the [deployed version](https://insect1285.gitlab.io/photologger/) to use.

# Inspiration
This project was inspired by my own tasks as a field scientist many years ago.  I wrote a basic
version in Excel using VBA, but as the tool grew in popularity internally, it became increasingly
difficult to maintain the VBA/versions of files that were "in the wild".  To help keep
all the users on the same version and to deploy patches more rapidly, I moved the the application
to a web app.  

This version is an overhaul and major update to the original, 
vanilla JS [Photolog WebApp](https://gitlab.com/insect1285/photolog_webapp), although that
version should still work in most major browsers.

This project is written with React and Typescript with the design of being a progressive web app (PWA) from the start.

# Features
* Users can upload photographs or saved files from the application and build a completed
photographic log in Word (docx) format.

# Customization
To customize, please fork this project and modify the RowData interface in App.tsx. 
The typescript errors will help you seek out the places where the keys are invalid, 
including in exportData, processFiles, buildDocuments, etc.

All docx construction is done in `src/helperFunctions/buildDocument` and it's imports.
The `buildDocument` and it's imports can be modified to match your data or to customize the export.

## Change Log
V 1.0.0
* Initial Release

V 1.0.1
* Users can move rows up and down.

V 1.0.2
* Project information (header inputs) are now saved and reloaded from zip.

V 1.0.3
* Bugfix: Images should now properly appear in the document. 
* All images are now resized when uploaded to a maximum size of 1080x1080 (aspect ratio is maintained).

## Road Map
v1.1.0 - Est. Mid-November
* Allow users to manipulate photographs (rotation, maybe crop)

v1.2.0 - Est. EOY 2019
* UI/UX Improvements to make data entry easier
* UI bug fixes
  * Fix row-resizing when editing/deleting

Long-term Support (LTS):
* Any addition requested features and bug fixes.
