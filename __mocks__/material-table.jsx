import React, { useEffect, useRef } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from 'prop-types';

const mockAddData = {
  imgName: 'favicon.ico',
  preview: `${process.env.PUBLIC_URL}/favicon.ico`,
  date: new Date(),
  direction: 'N',
  comments: 'Test Comment',
  tableData: {
    id: 0,
  },
};


function MaterialTable(props) {
  const {
    components, columns, editable, data, options, actions,
  } = props;

  const mounted = useRef(false);

  if (!mounted.current) {
    data.push(mockAddData);
  }

  useEffect(() => {
    mounted.current = true;
  }, []);

  const addRow = async () => {
    await editable.onRowAdd(mockAddData);
  };

  const updateRow = async () => {
    await editable.onRowUpdate({ ...mockAddData, comments: 'edit' }, data[0]);
  };

  const updateRowNoOldData = async () => {
    await editable.onRowUpdate({ ...mockAddData, comments: 'editOldData' }, null);
  };

  const removeRow = async () => {
    await editable.onRowDelete(data[0]);
  };

  return (
    <div>
      {Object.values(components).map((Component, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i}><Component /></div>
      ))}
      {data.length && columns.map((column, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i}>{column.render && column.render(data[0])}</div>
      ))}
      {data.length && editable && (
        <>
          <button type="button" title="Add" onClick={addRow}>Add Row</button>
          <button type="button" title="Delete" onClick={removeRow}>Delete Row</button>
          <button type="button" title="Edit" onClick={updateRow}>Edit Row</button>
          <button type="button" title="EditNoOldData" onClick={updateRowNoOldData}>Edit Row</button>
        </>
      )}
      {data.map((d, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i}>
          {Object.keys(d).map((k, ki) => (
            // eslint-disable-next-line react/no-array-index-key
            <div key={ki}>
              {d[k].toString()}
            </div>
          ))}
        </div>
      ))}
      {options.exportButton && (
        <button title="Export" type="button" onClick={options.exportCsv}>Export</button>
      )}
      {actions.map(action => (
        <button key={action.tooltip} title={action.tooltip} type="button" onClick={event => action.onClick(event, mockAddData)}>
          {action.icon()}
        </button>
      ))}
    </div>
  );
}

MaterialTable.propTypes = {
  components: PropTypes.shape({}),
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  editable: PropTypes.shape({
    onRowAdd: PropTypes.func,
    onRowDelete: PropTypes.func,
    onRowUpdate: PropTypes.func,
  }),
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  options: PropTypes.shape({
    exportButton: PropTypes.bool,
    exportCsv: PropTypes.func,
  }),
  actions: PropTypes.arrayOf(PropTypes.shape({
    icon: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    tooltip: PropTypes.string,
    onClick: PropTypes.func,
    isFreeAction: PropTypes.bool,
  })),
};

MaterialTable.defaultProps = {
  components: {},
  editable: {
    onRowAdd: () => {},
    onRowDelete: () => {},
    onRowUpdate: () => {},
  },
  options: {},
  actions: [],
};

export default MaterialTable;
