import React, { lazy, Suspense, useState } from 'react';
import { CircularProgress, makeStyles } from '@material-ui/core';

import AppHeader from './components/AppHeader/AppHeader';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import getHeaderInfo from './helperFunctions/getHeaderInfo';
import buildDocument from './helperFunctions/buildDocument';
import DownloadDialog from './components/DownloadDialog/DownloadDialog';

const LogHeaderInputs = lazy(() => import('./components/LogHeaderInputs/LogHeaderInputs'));
const LogPhotoInputs = lazy(() => import('./components/LogPhotoInputs/LogPhotoInputs'));

const useStyles = makeStyles({
  suspense: {
    display: 'flex',
    justifyContent: 'center',
  },
});

export interface RowData {
  imgName: string;
  preview: string;
  date: Date;
  direction: 'N' | 'NE' | 'E' | 'SE' | 'S' | 'SW' | 'W' | 'NW';
  comments: string;
}

export interface HeaderData {
  client: string;
  siteName: string;
  projectNumber: string;
  location: string;
}

function App() {
  const classes = useStyles();

  const [headerData, setHeaderData] = useState<HeaderData>({
    client: '',
    siteName: '',
    projectNumber: '',
    location: '',
  });
  const [data, setData] = useState<RowData[]>([]);
  const [buildLogOpen, setBuildLogOpen] = useState<boolean>(false);

  const buildLog = async () => {
    setBuildLogOpen(true);
    const headerInfo = getHeaderInfo();
    await buildDocument(headerInfo, data);
    setBuildLogOpen(false);
  };

  return (
    <ErrorBoundary>
      <AppHeader buildLog={buildLog} />
      <Suspense fallback={<div className={classes.suspense}><CircularProgress /></div>}>
        <LogHeaderInputs
          headerData={headerData}
          setHeaderData={setHeaderData}
        />
        <ErrorBoundary>
          <LogPhotoInputs
            data={data}
            setData={setData}
            setHeaderData={setHeaderData}
          />
        </ErrorBoundary>
        <DownloadDialog open={buildLogOpen} titleText="Building Log..." />
      </Suspense>
    </ErrorBoundary>
  );
}

export default App;
