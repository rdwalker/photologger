const originalFileReader = FileReader;

class MockFileReader extends FileReader {
  onload = () => {};

  onprogress = () => {};

  readAsDataUrl() {
    this.onload();
    this.onprogress({
      loaded: 10,
      total: 100,
    });
  }
}

window.URL.createObjectURL = jest.fn(() => '');

beforeAll(() => {
  window.FileReader = MockFileReader;
});

afterAll(() => {
  window.FileReader = originalFileReader;
});
