import React from 'react';
import {
  render, cleanup, act, fireEvent, wait,
} from '@testing-library/react';
import { saveAs } from 'file-saver';

import App from './App';
import getOutputDimensions from './helperFunctions/tableFunctions/tableRows/processImage';

jest.mock('file-saver');
jest.mock('./helperFunctions/tableFunctions/tableRows/processImage');
jest.mock('docx', () => ({
  ...jest.requireActual('docx'),
  Packer: {
    toBlob: async () => {},
  },
}));
jest.mock('utils/imageUtils');

describe('<App>', () => {
  afterEach(cleanup);
  const fileContents = 'file contents';
  const file = new File([fileContents], 'test.png', { type: 'image/png' });

  it('renders without crashing', () => {
    render(<App />);
  });

  describe('Uploading Files', () => {
    it('should upload files without error', async () => {
      const {
        getByTestId, getAllByText, getByText,
      } = render(<App />);
      await wait();

      await act(async () => {
        await fireEvent.change(getByTestId('file-input'), { target: { files: [file] } });
      });

      expect(getAllByText('test.png')).toHaveLength(2);

      fireEvent.click(getByText('Done'));
    });
  });

  it('should edit a row', async () => {
    const { getByTitle, queryByText } = render(<App />);
    await wait();

    await fireEvent.click(getByTitle('Edit'));

    expect(queryByText('Test Comment')).toBeNull();
    expect(queryByText('edit')).not.toBeNull();

    // Required for branch; This cannot actually edit anything though.
    await fireEvent.click(getByTitle('EditNoOldData'));
    // Nothing should change.
    expect(queryByText('edit')).not.toBeNull();
  });

  it('should delete a row', async () => {
    const { getByTitle, queryByText } = render(<App />);
    await wait();

    await fireEvent.click(getByTitle('Delete'));

    expect(queryByText('Test Comment')).toBeNull();
  });

  it('should build the log', async () => {
    (window.fetch as jest.Mock) = jest.fn()
      .mockResolvedValue({ arrayBuffer: () => new ArrayBuffer(8) });

    (getOutputDimensions as jest.Mock)
      .mockImplementation(async () => ({ width: 100, height: 100 }));

    const { getByTitle } = render(<App />);
    await wait(() => {
      fireEvent.click(getByTitle('Build Log'));
      expect(saveAs).toHaveBeenCalledTimes(1);
    });
  });

  describe('Header Inputs', () => {
    it('should update header inputs', async () => {
      const { getByPlaceholderText } = render(<App />);
      await wait();

      const clientInput = getByPlaceholderText('Client');
      if (!(clientInput instanceof HTMLInputElement)) {
        throw new Error('Not an input element');
      }

      fireEvent.change(clientInput, { target: { value: 'a' } });

      expect(clientInput.value).toBe('a');
    });
  });
});
