import React, { useRef, useState } from 'react';
import {
  AppBar, Typography, Toolbar, IconButton, Tooltip,
} from '@material-ui/core';
import { Help, Build } from '@material-ui/icons';

import HelpPopper from './Help/HelpPopper';

interface Props {
  buildLog: () => void;
}

function AppHeader({ buildLog }: Props) {
  const anchorRef = useRef<HTMLButtonElement | null>(null);
  const [helpOpen, setHelpOpen] = useState<boolean>(false);

  const closeHelpMenu = (event: React.MouseEvent<EventTarget>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }

    setHelpOpen(false);
  };

  const toggleHelp = () => {
    setHelpOpen(prevOpen => !prevOpen);
  };

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Typography variant="h4">
          Photologger
        </Typography>
        <div style={{ flexGrow: 1 }} />
        <Tooltip title="Build Log">
          <IconButton color="inherit" onClick={buildLog}>
            <Build />
          </IconButton>
        </Tooltip>
        <Tooltip title="Help">
          <IconButton color="inherit" ref={anchorRef} onClick={toggleHelp}>
            <Help />
          </IconButton>
        </Tooltip>
      </Toolbar>
      <HelpPopper anchorRef={anchorRef} helpOpen={helpOpen} closeHelpMenu={closeHelpMenu} />
    </AppBar>
  );
}

export default AppHeader;
