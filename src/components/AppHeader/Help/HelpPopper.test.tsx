import React from 'react';
import { render, cleanup } from '@testing-library/react';

import HelpPopper from './HelpPopper';

describe('<HelpPopper>', () => {
  const mockElement = document.createElement('button');
  const refMock = { current: mockElement };

  afterEach(cleanup);

  it('should render without error', () => {
    render(<HelpPopper anchorRef={refMock} helpOpen={false} closeHelpMenu={() => {}} />);
  });
});
