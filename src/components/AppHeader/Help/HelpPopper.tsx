import React from 'react';
import {
  List, ListItem, ListItemText, makeStyles, Popper, Grow, Paper, ClickAwayListener,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  listItem: {
    marginRight: theme.spacing(1),
  },
}));

interface Props {
  anchorRef: React.RefObject<HTMLButtonElement>;
  helpOpen: boolean;
  closeHelpMenu: (event: React.MouseEvent<EventTarget>) => void;
}

const instructions = [
  "Upload Photo(s) by clicking 'Add Photographs' and selecting your files.",
  'Arrange Photos and Add Details.',
  'Save Your Log (optional, but highly recommended).',
  'Generate Log and Save.',
];

function HelpPopper({ anchorRef, helpOpen, closeHelpMenu }: Props) {
  const classes = useStyles();

  return (
    <Popper open={helpOpen} anchorEl={anchorRef.current} transition disablePortal>
      {({ TransitionProps }) => (
        <Grow
          {...TransitionProps}
        >
          <Paper id="menu-list-grow" elevation={8}>
            <ClickAwayListener onClickAway={closeHelpMenu}>
              <List>
                <ListItem>
                  <ListItemText primary="Instructions" primaryTypographyProps={{ variant: 'h6' }} />
                </ListItem>
                {instructions.map((instruction, idx) => (
                  <ListItem key={instruction} className={classes.listItem}>
                    <ListItemText primary={`${idx + 1}. ${instruction}`} />
                  </ListItem>
                ))}
              </List>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  );
}

export default HelpPopper;
