import React from 'react';
import {
  render, cleanup, fireEvent,
} from '@testing-library/react';

import AppHeader from './AppHeader';

jest.useFakeTimers();

describe('<AppHeader>', () => {
  afterEach(cleanup);

  it('should render without error', () => {
    render(<AppHeader buildLog={() => {}} />);
  });

  it('should open the help popper, then close it when clicked again', () => {
    const { getByTitle, getByText, queryByText } = render(<AppHeader buildLog={() => {}} />);

    fireEvent.click(getByTitle('Help'));

    expect(getByText('Instructions')).not.toBeNull();

    fireEvent.click(getByTitle('Help'));
    jest.runAllTimers();

    expect(queryByText('Instructions')).toBeNull();
  });

  it('should open the help popper, then close it when clicked away', () => {
    const { getByTitle, getByText, queryByText } = render(<AppHeader buildLog={() => {}} />);

    fireEvent.click(getByTitle('Help'));

    expect(getByText('Instructions')).not.toBeNull();

    fireEvent.click(getByTitle('Build Log'));
    jest.runAllTimers();

    expect(queryByText('Instructions')).toBeNull();
  });

  it('should open the help popper and not close if the popover is clicked', () => {
    const { getByTitle, getByText } = render(<AppHeader buildLog={() => {}} />);

    fireEvent.click(getByTitle('Help'));

    expect(getByText('Instructions')).not.toBeNull();

    fireEvent.click(getByText('Instructions'));
    jest.runAllTimers();

    expect(getByText('Instructions')).not.toBeNull();
  });
});
