import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';

import ProgressDialog from './ProgressDialog';

describe('<ProgressDialog />', () => {
  afterEach(cleanup);
  it('should render without error', () => {
    render((
      <ProgressDialog
        modalOpen={false}
        closeModal={() => {}}
        className=""
        fileProgress={
          {
            test: {
              progress: 0,
              status: 'uploading',
              abortCallback: () => {},
            },
          }
        }
        uploadStatus={null}
      />
    ));
  });

  it('should close when "Done"', () => {
    const closeMock = jest.fn();
    const { getByText } = render((
      <ProgressDialog
        modalOpen
        closeModal={closeMock}
        className=""
        fileProgress={
          {
            test: {
              progress: 0,
              status: 'uploading',
              abortCallback: () => {},
            },
          }
        }
        uploadStatus="Done"
      />
    ));

    fireEvent.click(getByText('Done'));

    expect(closeMock).toHaveBeenCalledTimes(1);
  });
});
