import React from 'react';
import {
  Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography, makeStyles,
} from '@material-ui/core';

import Progress from './Progress/Progress';
import { FileProgress, UploadStatus } from '../LogPhotoInputs';

const useStyles = makeStyles(theme => ({
  statusButton: {
    position: 'absolute',
    right: theme.spacing(2),
    top: theme.spacing(2),
  },
}));

interface Props {
  modalOpen: boolean,
  closeModal: () => void;
  className: string,
  fileProgress: FileProgress,
  uploadStatus: UploadStatus
}

function ProgressDialog({
  modalOpen, closeModal, className, fileProgress, uploadStatus,
}: Props) {
  const classes = useStyles();

  return (
    <Dialog
      open={modalOpen}
      onClose={closeModal}
      disableBackdropClick
      PaperProps={{ className }}
    >
      <DialogTitle>
        Processing Files...
        {uploadStatus && (
        <DialogActions className={classes.statusButton}>
          <Button
            color="primary"
            variant="contained"
            disabled={uploadStatus === 'Uploading'}
            onClick={closeModal}
          >
            {uploadStatus}
          </Button>
        </DialogActions>
        )}
      </DialogTitle>
      <DialogContent>
        {Object.entries(fileProgress).map(([file, value]) => (
          <div key={file}>
            <Typography noWrap>
              {file}
            </Typography>
            <Progress
              progress={value.progress}
              status={value.status}
              abortUpload={value.abortCallback}
            />
          </div>
        ))}
      </DialogContent>
    </Dialog>
  );
}

export default ProgressDialog;
