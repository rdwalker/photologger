import React from 'react';
import {
  CircularProgress,
  IconButton, makeStyles, Theme,
} from '@material-ui/core';
import {
  Cancel, CancelOutlined, CheckCircle, Error,
} from '@material-ui/icons';

import { FileStatus } from '../../LogPhotoInputs';

interface Props {
  progress: number;
  status: FileStatus
  abortUpload: (() => void) | undefined;
}

const useStyles = makeStyles<Theme, Props>({
  ProgressWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  ProgressBar: {
    width: '100%',
    height: 8,
    backgroundColor: 'rgb(183, 155, 229)',
    borderRadius: 5,
  },
  Progress: {
    backgroundColor: 'rgba(103, 58, 183, 1)',
    height: '100%',
    margin: 0,
    borderRadius: 5,
    width: ({ progress }) => `${progress}%`,
  },
});

function Progress({ progress, status, abortUpload }: Props) {
  const classes = useStyles({ progress, status, abortUpload });

  return (
    <div className={classes.ProgressWrapper}>
      <div className={classes.ProgressBar}>
        <div
          className={classes.Progress}
        />
      </div>
      {status === 'uploading' && (
        <IconButton
          size="small"
          onClick={abortUpload}
          data-testid="cancel-button"
        >
          <CancelOutlined htmlColor="red" />
        </IconButton>
      )}
      {status === 'processing' && (
        <CircularProgress size={20} data-testid="processing-mark" />
      )}
      {status === 'done' && (
        <CheckCircle htmlColor="green" data-testid="done-mark" />
      )}
      {status === 'error' && (
        <Error htmlColor="red" data-testid="error-mark" />
      )}
      {status === 'cancelled' && (
        <Cancel htmlColor="red" data-testid="cancel-mark" />
      )}
      {status === 'pending' && (
        <CircularProgress size={20} color="secondary" data-testid="pending-mark" />
      )}
    </div>
  );
}

export default Progress;
