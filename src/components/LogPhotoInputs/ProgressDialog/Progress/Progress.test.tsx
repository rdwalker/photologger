import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';
import Progress from './Progress';

describe('<Progress>', () => {
  afterEach(cleanup);

  it('should render without error', () => {
    render(<Progress progress={0} status="uploading" abortUpload={() => {}} />);
  });

  it('should render with different status', () => {
    const { rerender, getByTestId } = render((
      <Progress
        progress={0}
        status="uploading"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('cancel-button')).not.toBeNull();

    rerender((
      <Progress
        progress={0}
        status="done"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('done-mark')).not.toBeNull();

    render((
      <Progress
        progress={0}
        status="error"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('error-mark')).not.toBeNull();

    render((
      <Progress
        progress={0}
        status="cancelled"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('cancel-mark')).not.toBeNull();

    render((
      <Progress
        progress={0}
        status="processing"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('processing-mark')).not.toBeNull();

    render((
      <Progress
        progress={0}
        status="pending"
        abortUpload={() => {}}
      />
    ));

    expect(getByTestId('pending-mark')).not.toBeNull();
  });

  it('should execute abortUpload when clicked', () => {
    const abortUploadMock = jest.fn();
    const { getByTestId } = render((
      <Progress
        progress={0}
        status="uploading"
        abortUpload={abortUploadMock}
      />
    ));

    fireEvent.click(getByTestId('cancel-button'));

    expect(abortUploadMock).toHaveBeenCalledTimes(1);
  });
});
