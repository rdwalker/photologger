import React, {
  lazy, Suspense, useCallback, useRef, useState,
} from 'react';
import pLimit from 'p-limit';
import MaterialTable from 'material-table';
import {
  CircularProgress, makeStyles, Paper, useTheme,
} from '@material-ui/core';
import { Add, ArrowUpward, ArrowDownward } from '@material-ui/icons';
import { ContainerProps } from '@material-ui/core/Container';

import tableIcons from 'helperFunctions/tableIcons';
import columns from 'helperFunctions/columns';
import { fileListToArray, processFile } from 'helperFunctions/processFiles';
import buildZip from 'helperFunctions/exportData';
import { HeaderData, RowData } from 'App';
import moveItem from 'utils/arrayUtils';

const ProgressDialog = lazy(() => import('./ProgressDialog/ProgressDialog'));
const DownloadDialog = lazy(() => import('../DownloadDialog/DownloadDialog'));

// TODO Allow re-order of rows
// TODO Allow images to be rotated

const useStyles = makeStyles(({
  FileInput: {
    display: 'none',
  },
  modal: {
    width: '40%',
  },
}));

export type FileStatus = 'pending' | 'uploading' | 'done' | 'error' | 'cancelled' | 'processing';
export type UploadStatus = 'Done' | 'Uploading' | null

export interface FileProgress {
  [name: string]: {
    progress: number,
    status: FileStatus,
    abortCallback: (() => void) | undefined,
  }
}

interface Props {
  data: RowData[];
  setData: React.Dispatch<React.SetStateAction<RowData[]>>;
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>;
}

function LogPhotoInputs({ data, setData, setHeaderData }: Props) {
  const classes = useStyles();
  const theme = useTheme();

  const fileInputRef = useRef<HTMLInputElement>(document.createElement('input'));

  const [fileProgress, setFileProgress] = useState<FileProgress>({});
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [downloadOpen, setDownloadOpen] = useState<boolean>(false);
  const [uploadStatus, setUploadStatus] = useState<UploadStatus>(null);
  const [pageNumber, setPageNumber] = useState(0);
  const [pageSize, setPageSize] = useState(5);

  const closeModal = () => {
    setModalOpen(false);
  };

  const openFileDialog = () => {
    fileInputRef.current.click();
  };

  const handleFilesAdded = async (evt: React.ChangeEvent<HTMLInputElement>) => {
    const { files } = evt.target;
    if (files) {
      setUploadStatus('Uploading');

      const promises: Promise<void>[] = [];

      setModalOpen(true);
      const array = fileListToArray(files, setFileProgress);
      const limit = pLimit(5);

      array.forEach((file) => {
        promises.push(limit(() => processFile(file, setFileProgress, setData, setHeaderData)));
      });

      await Promise.all(promises);
      fileInputRef.current.value = '';
      setUploadStatus('Done');
    }
  };

  const exportTableToCSV = useCallback(async (columnData) => {
    setDownloadOpen(true);
    await buildZip(columnData, data);
    setDownloadOpen(false);
  }, [data]);

  const moveRowUp = (_: Event, row: RowData | RowData[]) => {
    setData(prevData => (
      // @ts-ignore RowData has TableData appended by MaterialTable
      // but types are wrong in MaterialTable
      // and row cannot be RowData[] because the action is not a free action
      [...moveItem<RowData>(prevData, row.tableData.id, row.tableData.id - 1)]));
  };

  const moveRowDown = (_: Event, row: RowData | RowData[]) => {
    setData(prevData => (
      // @ts-ignore RowData has TableData appended by MaterialTable
      // but types are wrong in MaterialTable
      // and row cannot be RowData[] because the action is not a free action
      [...moveItem<RowData>(prevData, row.tableData.id, row.tableData.id + 1)]));
  };

  return (
    <>
      <input
        ref={fileInputRef}
        className={classes.FileInput}
        type="file"
        accept="image/*, .zip"
        multiple
        onChange={handleFilesAdded}
        data-testid="file-input"
      />
      <MaterialTable
        icons={tableIcons}
        components={{
          Container: (props: ContainerProps) => (
            <Paper {...props} square style={{ margin: theme.spacing(1) }} />
          ),
        }}
        options={{
          showTitle: false,
          actionsColumnIndex: -1,
          pageSize,
          initialPage: pageNumber,
          pageSizeOptions: [5, 10, 20, 50, 100],
          exportAllData: true,
          exportButton: true,
          exportCsv: exportTableToCSV,
        }}
        localization={{
          body: {
            editTooltip: 'Edit',
          },
          toolbar: {
            exportName: 'Export to Zip',
          },
        }}
        columns={columns}
        data={data}
        actions={[
          {
            icon: () => <Add />,
            tooltip: 'Add Photograph',
            onClick: openFileDialog,
            isFreeAction: true,
          },
          {
            icon: () => <ArrowUpward />,
            tooltip: 'Move Row Up',
            onClick: moveRowUp,
            isFreeAction: false,
          },
          {
            icon: () => <ArrowDownward />,
            tooltip: 'Move Row Down',
            onClick: moveRowDown,
            isFreeAction: false,
          },
        ]}
        editable={{
          onRowUpdate: (newData, oldData) => (
            new Promise((resolve) => {
              if (oldData) {
                const index = data.indexOf(oldData);
                const copy = [...data];
                copy[index] = { ...newData, date: new Date(newData.date) };
                setData(copy);
                resolve();
              }
            })),
          onRowDelete: oldData => (
            new Promise((resolve) => {
              const index = data.indexOf(oldData);
              data.splice(index, 1);
              setData([...data]);
              resolve();
            })),
        }}
        onChangePage={setPageNumber}
        onChangeRowsPerPage={setPageSize}
      />
      <Suspense fallback={<CircularProgress />}>
        <ProgressDialog
          modalOpen={modalOpen}
          closeModal={closeModal}
          className={classes.modal}
          fileProgress={fileProgress}
          uploadStatus={uploadStatus}
        />
        <DownloadDialog
          open={downloadOpen}
          titleText="Zipping Files..."
        />
      </Suspense>
    </>
  );
}

export default LogPhotoInputs;
