import React from 'react';
import {
  render, cleanup, fireEvent, act,
} from '@testing-library/react';

import * as exportData from 'helperFunctions/exportData';
import moveItem from 'utils/arrayUtils';
import LogPhotoInputs from './LogPhotoInputs';

jest.mock('helperFunctions/exportData');
jest.mock('utils/arrayUtils');
jest.mock('utils/imageUtils');

describe('<LogPhotoInputs>', () => {
  const setDataMock = jest.fn();

  afterEach(() => {
    cleanup();
    setDataMock.mockClear();
    (moveItem as jest.Mock).mockClear();
  });

  const fileContents = 'file contents';
  const file = new File([fileContents], 'test.png', { type: 'image/png' });

  it('should render without error', () => {
    jest.unmock('material-table');
    render(<LogPhotoInputs data={[]} setData={jest.fn()} setHeaderData={jest.fn()} />);
  });

  describe('Uploading Files', () => {
    it('should upload files without error', async () => {
      const { getByTestId, getByText, getByTitle } = render((
        <LogPhotoInputs data={[]} setData={setDataMock} setHeaderData={jest.fn()} />));

      fireEvent.click(getByTitle('Add Photograph'));
      await act(async () => {
        await fireEvent.change(getByTestId('file-input'), { target: { files: [file] } });
      });

      expect(setDataMock).toHaveBeenCalledTimes(1);

      fireEvent.click(getByText('Done'));
    });

    it('should not upload files if none selected', async () => {
      const { getByTestId, getByTitle } = render((
        <LogPhotoInputs data={[]} setData={setDataMock} setHeaderData={jest.fn()} />));

      fireEvent.click(getByTitle('Add Photograph'));
      await act(async () => {
        await fireEvent.change(getByTestId('file-input'), { target: { files: null } });
      });

      expect(setDataMock).toHaveBeenCalledTimes(0);
    });
  });

  it('should move a row up', async () => {
    const setDataMoveMock = jest.fn((fn) => { fn(); });
    const { getByTitle } = render((
      <LogPhotoInputs data={[]} setData={setDataMoveMock} setHeaderData={jest.fn()} />
    ));

    await fireEvent.click(getByTitle('Move Row Up'));
    expect(setDataMoveMock).toHaveBeenCalledTimes(1);
    expect(moveItem).toHaveBeenCalledTimes(1);
  });

  it('should move a row down', async () => {
    const setDataMoveMock = jest.fn((fn) => { fn(); });
    const { getByTitle } = render((
      <LogPhotoInputs data={[]} setData={setDataMoveMock} setHeaderData={jest.fn()} />
    ));

    await fireEvent.click(getByTitle('Move Row Down'));
    expect(setDataMoveMock).toHaveBeenCalledTimes(1);
    expect(moveItem).toHaveBeenCalledTimes(1);
  });

  it('should edit a row', async () => {
    const { getByTitle } = render((
      <LogPhotoInputs data={[]} setData={setDataMock} setHeaderData={jest.fn()} />
    ));

    await fireEvent.click(getByTitle('Edit'));
    expect(setDataMock).toHaveBeenCalledTimes(1);

    // Required for branch; This cannot actually edit anything though.
    await fireEvent.click(getByTitle('EditNoOldData'));
    // Nothing should change.
    expect(setDataMock).toHaveBeenCalledTimes(1);
  });

  it('should delete a row', async () => {
    const { getByTitle } = render((
      <LogPhotoInputs data={[]} setData={setDataMock} setHeaderData={jest.fn()} />));

    await fireEvent.click(getByTitle('Delete'));
    expect(setDataMock).toHaveBeenCalledTimes(1);
  });

  it('should export to Zip', async () => {
    const { getByTitle } = render((
      <LogPhotoInputs data={[]} setData={setDataMock} setHeaderData={jest.fn()} />));

    await act(async () => {
      fireEvent.click(getByTitle('Export'));
    });

    expect(exportData.default).toHaveBeenCalledTimes(1);
  });
});
