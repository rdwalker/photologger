import React from 'react';
import {
  Paper, Grid, TextField, Typography, makeStyles,
} from '@material-ui/core';

import { HeaderData } from 'App';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
  },
}));

interface Props {
  headerData: HeaderData;
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>;
}

function LogHeaderInputs({ headerData, setHeaderData }: Props) {
  const classes = useStyles();

  const handleChange = (key: string) => (event: React.ChangeEvent<HTMLInputElement>) => {
    setHeaderData(prevData => ({
      ...prevData,
      [key]: event.target.value,
    }));
  };

  return (
    <Paper className={classes.paper} square>
      <Typography variant="h6">
        Log Header:
      </Typography>
      <Grid container spacing={2}>
        <Grid item>
          <TextField
            id="input-client"
            placeholder="Client"
            label="Client"
            value={headerData.client}
            onChange={handleChange('client')}
          />
        </Grid>
        <Grid item>
          <TextField
            id="input-site-name"
            placeholder="Site Name"
            label="Site Name"
            value={headerData.siteName}
            onChange={handleChange('siteName')}
          />
        </Grid>
        <Grid item>
          <TextField
            id="input-project-number"
            placeholder="Project Number"
            label="Project Number"
            value={headerData.projectNumber}
            onChange={handleChange('projectNumber')}
          />
        </Grid>
        <Grid item>
          <TextField
            id="input-location"
            placeholder="Location"
            label="Location"
            value={headerData.location}
            onChange={handleChange('location')}
          />
        </Grid>
      </Grid>
    </Paper>
  );
}

export default LogHeaderInputs;
