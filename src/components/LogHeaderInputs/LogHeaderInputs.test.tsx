import React from 'react';
import { render, cleanup, fireEvent } from '@testing-library/react';

import { HeaderData } from 'App';
import LogHeaderInputs from './LogHeaderInputs';

describe('<LogHeaderInputs>', () => {
  const mockHeaderData: HeaderData = {
    client: '',
    projectNumber: '',
    siteName: '',
    location: '',
  };

  const setHeaderDataMock = jest.fn();

  afterEach(() => {
    cleanup();
    setHeaderDataMock.mockClear();
  });

  it('should render without error', () => {
    render(<LogHeaderInputs headerData={mockHeaderData} setHeaderData={setHeaderDataMock} />);
  });

  it('should update the header inputs', () => {
    const { getByPlaceholderText } = render(
      <LogHeaderInputs
        headerData={mockHeaderData}
        setHeaderData={setHeaderDataMock}
      />,
    );

    fireEvent.change(getByPlaceholderText('Client'), { target: { value: 'a' } });

    expect(setHeaderDataMock).toHaveBeenCalledTimes(1);
  });
});
