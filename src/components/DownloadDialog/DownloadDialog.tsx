import React from 'react';
import {
  Dialog, DialogContent, LinearProgress, makeStyles, Typography,
} from '@material-ui/core';

const useStyles = makeStyles({
  downloadModal: {
    display: 'flex',
    flexDirection: 'column',
    '& > h6, h4': {
      alignSelf: 'center',
    },
  },
});

interface Props {
  open: boolean;
  titleText: string,
}

function DownloadDialog({ open, titleText }: Props) {
  const classes = useStyles();

  return (
    <Dialog open={open}>
      <DialogContent className={classes.downloadModal}>
        <Typography variant="h4">
          {titleText}
        </Typography>
        <Typography variant="h6">
          Download will start automatically when done.
        </Typography>
        <LinearProgress />
      </DialogContent>
    </Dialog>
  );
}

export default DownloadDialog;
