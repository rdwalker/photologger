import React, { useEffect } from 'react';
import { render, cleanup } from '@testing-library/react';
import ErrorBoundary from './ErrorBoundary';

describe('<ErrorBoundary />', () => {
  afterEach(cleanup);

  it('should render without error', () => {
    render(<ErrorBoundary><div /></ErrorBoundary>);
  });

  it('should render if component errors during render', () => {
    // Silence console.error to keep test output clear;
    const consoleSpy = jest.spyOn(console, 'error');
    consoleSpy.mockImplementation(() => {});

    const BuggyChild = () => {
      useEffect(() => {
        throw new Error('Crash!');
      }, []);

      return (
        <div />
      );
    };

    const { getByText } = render(<ErrorBoundary><BuggyChild /></ErrorBoundary>);

    expect(getByText(/Something went wrong/)).not.toBeNull();
    expect(consoleSpy).toHaveBeenCalledTimes(2);

    consoleSpy.mockRestore();
  });
});
