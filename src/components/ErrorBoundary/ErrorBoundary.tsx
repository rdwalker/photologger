import React, { ReactNode } from 'react';

interface Props {
  readonly children: ReactNode
}

interface State {
  readonly hasError: boolean
}

class ErrorBoundary extends React.Component<Props, State> {
  state = { hasError: false };

  static getDerivedStateFromError(error: Error) {
    return { hasError: true };
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;
    if (hasError) {
      return <h1>Oops! Something went wrong.  Please refresh the page.</h1>;
    }

    return children;
  }
}

export default ErrorBoundary;
