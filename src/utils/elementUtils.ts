const getInputValue = (element: HTMLElement | null) => (
  element && element instanceof HTMLInputElement ? element.value : ''
);

export default getInputValue;
