import moveItem from './arrayUtils';

describe('Array Utils', () => {
  describe('moveItem', () => {
    it('should move an array item', () => {
      const originalData = [1, 2, 3];

      const movedResult = moveItem<number>(originalData, 0, 1);

      expect(movedResult).toEqual([2, 1, 3]);
    });
  });
});
