import getInputValue from './elementUtils';

describe('Element Utils', () => {
  describe('getInputValue', () => {
    it('should return input value if given an HTMLInputElement', () => {
      const inputElement = document.createElement('input');
      inputElement.type = 'text';
      inputElement.value = 'test';

      expect(getInputValue(inputElement)).toEqual('test');
    });

    it('should return "" if given null or non-HTMLInputElement', () => {
      expect(getInputValue(null)).toEqual('');

      const divElement = document.createElement('div');

      expect(getInputValue(divElement)).toEqual('');
    });
  });
});
