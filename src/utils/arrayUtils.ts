
function moveItem<T>(items: T[], from: number, to: number) {
  const itemBeingMoved = items[from];
  items.splice(from, 1);
  items.splice(to, 0, itemBeingMoved);

  return items;
}

export default moveItem;
