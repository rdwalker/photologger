import resizeImage from './imageUtils';

jest.mock('wasm-imagemagick', () => ({
  Call: () => [{ blob: 'test' }],
}));

describe('Image Utils', () => {
  describe('resizeImage', () => {
    it('should resize an image successfully', async () => {
      (window.fetch as jest.Mock) = jest.fn(() => new Response(new Blob()));

      const fileContents = 'file contents';
      const file = new File([fileContents], 'test.png', { type: 'image/png' });
      const blobURL = await window.URL.createObjectURL(file);

      const result = await resizeImage(blobURL);
      expect(result[0].blob).toEqual('test');
    });
  });
});
