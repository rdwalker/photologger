// @ts-ignore
import { Call } from 'wasm-imagemagick';

async function resizeImage(
  srcImage: string,
): Promise<{blob: Blob}[]> {
  const fetchedSourceImage = await fetch(srcImage);
  const arrayBuffer = await fetchedSourceImage.arrayBuffer();
  const sourceBytes = new Uint8Array(arrayBuffer);

  const files = [{ name: 'srcFile.png', content: sourceBytes }];
  const command = ['convert', 'srcFile.png', '-resize', '1080x1080>', 'out.png'];

  return Call(files, command);
}

export default resizeImage;
