import getHeaderInfo from './getHeaderInfo';

describe('getHeaderInfo', () => {
  it('should return a list from the inputs', () => {
    document.body.innerHTML = '<input id="input-client" value="123" />';

    expect(getHeaderInfo().client.value).toEqual('123');
    expect(getHeaderInfo().location.value).toEqual('');
  });
});
