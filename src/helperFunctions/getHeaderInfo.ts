export interface HeaderInfo {
  [key: string]: { value: string, readonly inputId: string }
}

const getHeaderInfo = (): HeaderInfo => {
  const headerInfo: HeaderInfo = {
    client: {
      value: '',
      inputId: 'input-client',
    },
    siteName: {
      value: '',
      inputId: 'input-site-name',
    },
    projectNumber: {
      value: '',
      inputId: 'input-project-number',
    },
    location: {
      value: '',
      inputId: 'input-location',
    },
  };

  Object.entries(headerInfo).forEach(([key, value]) => {
    const input = document.getElementById(value.inputId);
    if (input && input instanceof HTMLInputElement) {
      headerInfo[key].value = input.value;
    }
  });

  return headerInfo;
};

export default getHeaderInfo;
