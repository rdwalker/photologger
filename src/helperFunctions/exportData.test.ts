import * as FileSaver from 'file-saver';

import buildZip from './exportData';

jest.mock('file-saver', () => ({
  saveAs: jest.fn(),
}));

describe('exportData', () => {
  it('should build a zip file from the data', async () => {
    window.fetch = jest.fn().mockReturnValue({ blob: () => {} });

    await buildZip(
      [
        {
          field: 'imgName',
        },
      ],
      [
        {
          imgName: 'favicon.ico',
          preview: `${process.env.PUBLIC_URL}/favicon.ico`,
          date: new Date(),
          direction: 'N',
          comments: 'Test Comment',
        },
      ],
    );

    expect(FileSaver.saveAs).toHaveBeenCalledTimes(1);
  });
});
