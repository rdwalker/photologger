import React from 'react';
import JSZip from 'jszip';
import csvtojson from 'csvtojson';
import debounce from 'debounce';

import { FileProgress } from 'components/LogPhotoInputs/LogPhotoInputs';
import { HeaderData, RowData } from 'App';
import resizeImage from '../utils/imageUtils';

const loadImage = async (
  file: File,
  setFileProgress: React.Dispatch<React.SetStateAction<FileProgress>>,
  setData: React.Dispatch<React.SetStateAction<RowData[]>>,
) => {
  const blobURL = await window.URL.createObjectURL(file);

  const previewImage = await resizeImage(blobURL);

  const previewURL = await window.URL.createObjectURL(previewImage[0].blob);

  setData(prevData => ([
    ...prevData,
    {
      imgName: file.name,
      preview: previewURL,
      date: new Date(file.lastModified),
      direction: 'N',
      comments: '',
    },
  ]));

  setFileProgress(prevProgress => ({
    ...prevProgress,
    [file.name]: {
      progress: 100,
      status: 'done',
      abortCallback: undefined,
    },
  }));
};

const loadCsv = async (
  fileName: string,
  filePath: string,
  zip: JSZip,
  setData: React.Dispatch<React.SetStateAction<RowData[]>>,
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>,
): Promise<void> => {
  const csvData = await zip.file(filePath).async('text');
  await csvtojson()
    .fromString(csvData)
    .subscribe(async (json) => {
      if (fileName.includes('Data')) {
        const imgBlob = await zip
          .file(json.imgName)
          .async('blob');

        setData(prevData => ([
          ...prevData,
          {
            ...json,
            date: new Date(json.date),
            preview: window.URL.createObjectURL(imgBlob),
          },
        ]));
      } else {
        setHeaderData({
          client: json.Client,
          projectNumber: json['Project Number'],
          siteName: json['Site Name'],
          location: json.Location,
        });
      }
    });
};

const loadZip = async (
  file: File,
  setFileProgress: React.Dispatch<React.SetStateAction<FileProgress>>,
  setData: React.Dispatch<React.SetStateAction<RowData[]>>,
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>,
) => {
  const zip = new JSZip();
  const zipData = await zip.loadAsync(file);
  const promises: Promise<void>[] = [];

  zipData.forEach((relativePath, entry) => {
    if (entry.name.includes('.csv')) {
      promises.push(loadCsv(entry.name, relativePath, zipData, setData, setHeaderData));
    }
  });

  await Promise.all(promises);
  setFileProgress(prevProgress => ({
    ...prevProgress,
    [file.name]: {
      progress: 100,
      status: 'done',
      abortCallback: undefined,
    },
  }));
};

const readFileAsync = async (
  file: File,
  setFileProgress: React.Dispatch<React.SetStateAction<FileProgress>>,
  setData: React.Dispatch<React.SetStateAction<RowData[]>>,
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>,
) => (
  new Promise((resolve) => {
    const reader = new FileReader();

    const abortUpload = () => {
      reader.abort();
    };

    const onProgress = debounce((e: ProgressEvent) => {
      setFileProgress(prevProgress => ({
        ...prevProgress,
        [file.name]: {
          progress: Math.round((e.loaded / e.total) * 100),
          status: 'uploading',
          abortCallback: abortUpload,
        },
      }));
    }, 500, true);

    reader.onabort = (e) => {
      setFileProgress(prevProgress => ({
        ...prevProgress,
        [file.name]: {
          progress: Math.round((e.loaded / e.total) * 100),
          status: 'cancelled',
          abortCallback: abortUpload,
        },
      }));
      resolve('cancelled');
    };

    reader.onprogress = onProgress;

    reader.onload = async () => {
      onProgress.clear();

      setFileProgress(prevProgress => ({
        ...prevProgress,
        [file.name]: {
          progress: 100,
          status: 'processing',
          abortCallback: undefined,
        },
      }));
      const imageType = /^image\//;

      if (imageType.test(file.type)) {
        await loadImage(file, setFileProgress, setData);
      } else if (file.type === 'application/x-zip-compressed') {
        await loadZip(file, setFileProgress, setData, setHeaderData);
      } else {
        abortUpload();
        setFileProgress(prevProgress => ({
          ...prevProgress,
          [file.name]: {
            progress: 0,
            status: 'error',
            abortCallback: undefined,
          },
        }));
        resolve('Incorrect file type');
      }

      resolve(reader.result);
    };

    reader.onerror = () => {
      setFileProgress(prevProgress => ({
        ...prevProgress,
        [file.name]: {
          progress: 0,
          status: 'error',
          abortCallback: undefined,
        },
      }));
      abortUpload();
      resolve(`Error reading ${file.name}`);
    };

    reader.readAsDataURL(file);
  }));

const processFile = async (
  file: File,
  setFileProgress: React.Dispatch<React.SetStateAction<FileProgress>>,
  setData: React.Dispatch<React.SetStateAction<RowData[]>>,
  setHeaderData: React.Dispatch<React.SetStateAction<HeaderData>>,
) => {
  await readFileAsync(file, setFileProgress, setData, setHeaderData);
};

const fileListToArray = (
  fileList: FileList,
  setFileProgress: React.Dispatch<React.SetStateAction<FileProgress>>,
): File[] => {
  const array = [];

  for (let i = 0; i < fileList.length; i += 1) {
    const file = fileList[i];
    setFileProgress(prevProgress => (
      {
        ...prevProgress,
        [file.name]:
            {
              progress: 0,
              status: 'pending',
              abortCallback: undefined,
            },
      }));

    array.push(file);
  }
  return array;
};

export { processFile, fileListToArray };
