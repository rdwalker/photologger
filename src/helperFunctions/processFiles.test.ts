import JSZip from 'jszip';
import { fireEvent } from '@testing-library/react';

import { processFile, fileListToArray } from './processFiles';

jest.mock('utils/imageUtils');

describe('processFiles', () => {
  const setFileProgressMock = jest.fn((func) => {
    func({});
  });
  const setDataMock = jest.fn((func) => {
    func([]);
  });

  const setHeaderDataMock = jest.fn();

  afterEach(() => {
    setFileProgressMock.mockClear();
    setDataMock.mockClear();
  });

  describe('processFile', () => {
    it('should read an image file and add it to data', async () => {
      const fileContents = 'file contents';
      const file = new File([fileContents], 'test.png', { type: 'image/png' });

      await processFile(
        file,
        setFileProgressMock,
        setDataMock,
        setHeaderDataMock,
      );

      expect(setDataMock).toHaveBeenCalledTimes(1);
    });

    it('should read a zip file and add its contents to data and headerData', async () => {
      const zip = new JSZip();

      zip.file('test.png', new File(
        [new Blob()],
        'test.png',
        { type: 'image/png' },
      ));
      zip.file('testData.csv', 'imgName\ntest.png');
      zip.file('testInfo.csv', 'Client,\nABC');

      const file = new File(
        [await zip.generateAsync({ type: 'blob' })],
        'test.zip',
        { type: 'application/x-zip-compressed' },
      );

      await processFile(
        file,
        setFileProgressMock,
        setDataMock,
        setHeaderDataMock,
      );

      expect(setDataMock).toHaveBeenCalledTimes(1);
      expect(setHeaderDataMock).toHaveBeenCalledTimes(1);
    });

    it('should not read other file types', async () => {
      const file = new File(
        [],
        'test.zip',
        { type: 'application/xyz' },
      );

      await processFile(
        file,
        setFileProgressMock,
        setDataMock,
        setHeaderDataMock,
      );

      expect(setFileProgressMock).toHaveBeenCalledTimes(3);
    });

    it('should abort reading a file', async () => {
      const fileContents = 'file contents';
      const file = new File([fileContents], 'test.png', { type: 'image/png' });

      setFileProgressMock.mockImplementationOnce((func) => {
        // When progress is called - trigger an abort.
        func()['test.png'].abortCallback();
      });

      await processFile(
        file,
        setFileProgressMock,
        setDataMock,
        setHeaderDataMock,
      );

      expect(setDataMock).toHaveBeenCalledTimes(0);
    });

    it('should set status error and abort if fileReader has an error', async () => {
      const file = new File([], 'test.png', { type: 'image/png' });

      class MockFileReader extends FileReader {
        onerror = () => {};

        onloadstart =() => {
          this.onerror();
        };

        readAsDataUrl() {
          this.onloadstart();
        }
      }

      interface WindowWithFileReader extends Window {
        FileReader: any;
      }

      (window as WindowWithFileReader).FileReader = MockFileReader;

      await processFile(
        file,
        setFileProgressMock,
        setDataMock,
        setHeaderDataMock,
      );

      expect(setDataMock).toHaveBeenCalledTimes(0);
    });
  });

  describe('fileListToArray', () => {
    it('should convert FileList to array of Files', () => {
      const fileContents = 'file contents';
      const file = new File([fileContents], 'test.png', { type: 'image/png' });

      const input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('multiple', 'true');

      input.onchange = (event: Event) => {
        if (!(event.target instanceof HTMLInputElement)) throw new Error('Not input element');
        if (event.target && event.target.files) {
          expect(fileListToArray(event.target.files, setFileProgressMock)).toEqual([file]);
        }
      };

      fireEvent.change(input, { target: { files: [file] } });

      expect(setFileProgressMock).toHaveBeenCalledTimes(1);
    });

    it('should return empty array if no files', () => {
      const input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('multiple', 'true');
      input.value = '';

      if (!input.files) throw new Error('No files');
      expect(fileListToArray(input.files, setFileProgressMock)).toEqual([]);
      expect(setFileProgressMock).not.toHaveBeenCalled();
    });
  });
});
