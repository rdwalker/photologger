import JSZip from 'jszip';
import { saveAs } from 'file-saver';
import { Column } from 'material-table';

import { RowData } from 'App';
import getInputValue from 'utils/elementUtils';

const buildZip = async (columnData: Column<RowData>[], data: RowData[]) => {
  const zip = new JSZip();
  let csv = '';
  let projectInfoCSV = '';

  projectInfoCSV += 'Client, Site Name, Project Number, Location,\n';
  projectInfoCSV += `${getInputValue(document.getElementById('input-client'))},`;
  projectInfoCSV += `${getInputValue(document.getElementById('input-site-name'))},`;
  projectInfoCSV += `${getInputValue(document.getElementById('input-project-number'))},`;
  projectInfoCSV += `${getInputValue(document.getElementById('input-location'))},`;

  // Build Headers
  columnData.forEach((col) => {
    csv += `${col.field},`;
  });
  csv += '\n';

  const dataToZip = async () => {
    const results: Promise<void>[] = [];

    const zipData = async (datum: RowData) => {
      const img = await fetch(datum.preview);
      zip.file(datum.imgName, img.blob(), { base64: true });
      csv += `"${datum.imgName}",`;
      csv += `"${datum.preview}",`;
      csv += `"${datum.date}",`;
      csv += `"${datum.direction}",`;
      csv += `"${datum.comments}",`;
      csv += '\n';
    };

    data.forEach((datum) => {
      results.push(zipData(datum));
    });

    return Promise.all(results);
  };

  await dataToZip();

  const saveDate = new Date();
  const saveName = `Photologger_${saveDate.getFullYear()}${saveDate.getMonth()}${saveDate.getDate()}${saveDate.getHours()}${saveDate.getMinutes()}`;

  zip.file(`${saveName}Info.csv`, projectInfoCSV);
  zip.file(`${saveName}Data.csv`, csv);

  await zip.generateAsync({ type: 'blob' }).then((imgZip) => {
    saveAs(imgZip, `${saveName}.zip`);
  });
};

export default buildZip;
