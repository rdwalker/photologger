import { saveAs } from 'file-saver';

import buildDocument, { convertInchesToDXA } from './buildDocument';
import { HeaderInfo } from './getHeaderInfo';
import { RowData } from '../App';
import getOutputDimensions from './tableFunctions/tableRows/processImage';

jest.mock('file-saver');
jest.mock('./tableFunctions/tableRows/processImage');
jest.mock('docx', () => ({
  ...jest.requireActual('docx'),
  Packer: {
    toBlob: async () => {},
  },
}));

const buildRowData = (numberofRows: number = 1): RowData[] => {
  const data: RowData[] = [];

  for (let i = 0; i < numberofRows; i += 1) {
    data.push({
      imgName: `Test${i}.jpg`,
      preview: 'Test.jpg',
      date: new Date(),
      direction: 'N',
      comments: 'Test Comments',
    });
  }

  return data;
};

describe('buildDocument', () => {
  (window.fetch as jest.Mock) = jest.fn()
    .mockResolvedValue({ arrayBuffer: () => new ArrayBuffer(8) });

  (getOutputDimensions as jest.Mock)
    .mockImplementation(async () => ({ width: 100, height: 100 }));

  afterEach(() => {
    (saveAs as jest.Mock).mockClear();
  });

  it('should convert inches to DXA', () => {
    expect(convertInchesToDXA(1)).toEqual(72 * 20);
  });

  const headerInfo: HeaderInfo = {
    client: {
      value: '1',
      inputId: 'input-client',
    },
    siteName: {
      value: '2',
      inputId: 'input-site-name',
    },
    projectNumber: {
      value: '3',
      inputId: 'input-project-number',
    },
    location: {
      value: '4',
      inputId: 'input-location',
    },
  };

  it('should save a document with the header info and data given', async () => {
    const data = buildRowData(2);

    await buildDocument(headerInfo, data);

    expect(saveAs).toHaveBeenCalledTimes(1);
  });

  it('should save a document with an odd number of photos that only has half table on the last page', async () => {
    const data = buildRowData(3);

    await buildDocument(headerInfo, data);

    expect(saveAs).toHaveBeenCalledTimes(1);
  });
});
