import {
  Document, Footer, ITextRunOptions, Packer,
  Paragraph, TabStopPosition, TabStopType, TextRun, WidthType,
  Table, BorderStyle, TableRow,
} from 'docx';
import { saveAs } from 'file-saver';

import { RowData } from 'App';
import makeTitleRow from './tableFunctions/tableRows/makeTitleRow';
import { HeaderInfo } from './getHeaderInfo';
import makeSubHeaderRow from './tableFunctions/tableRows/makeSubHeaderRow';

import makeDataRow from './tableFunctions/tableRows/makeDataRows';

// 72 points per inch, DXA is 1/20th of a point.
export const convertInchesToDXA = (inches: number) => inches * 72 * 20;

export const imageCellHeight = 3.37;
export const imageCellWidth = 4.15;

export const borderStyle = {
  size: 6,
  style: BorderStyle.THICK,
  color: 'black',
};

const buildDocument = async (headerInfo: HeaderInfo, data: RowData[]) => {
  const saveDate = new Date();
  const saveName = `Photologger_${saveDate.getFullYear()}${saveDate.getMonth()}${saveDate.getDate()}${saveDate.getHours()}${saveDate.getMinutes()}.docx`;
  const doc = new Document();

  const numberOfPages = Math.ceil(data.length / 2); // Two photos per page.

  let dataIndex = 0;
  const rowPromises = [];

  let tableRows = [
    makeTitleRow(),
    makeSubHeaderRow(
      'Client',
      headerInfo.client.value,
      'Project Number',
      headerInfo.projectNumber.value,
    ),
    makeSubHeaderRow(
      'Site Name',
      headerInfo.siteName.value,
      'Site Location',
      headerInfo.location.value,
    ),
  ];

  for (let i = 0; i < numberOfPages; i += 1) {
    rowPromises.push(makeDataRow(doc, data[dataIndex], dataIndex + 1));
    dataIndex += 1;

    if (!(i + 1 === numberOfPages && !!(data.length % 2))) {
      rowPromises.push(makeDataRow(doc, data[i + 1], dataIndex + 1));
      dataIndex += 1;
    }
  }

  const promisedRows = await Promise.all(rowPromises);
  // Prefer to use .flat(), but jest does not support it yet.
  const flattenedRows = ([] as TableRow[]).concat(...promisedRows);
  tableRows = tableRows.concat(flattenedRows);

  const textOptions: ITextRunOptions = {
    text: '',
    font: { name: 'Times New Roman' },
    size: 16,
  };

  const pageNumberText = new TextRun({
    ...textOptions,
    text: 'Page ',
  }).pageNumber();

  const pagesText = new TextRun({
    ...textOptions,
    text: ' of ',
  }).numberOfTotalPages();

  const dateText = new TextRun({
    ...textOptions,
    text: `${saveDate.getFullYear()}.${saveDate.getMonth()}.${saveDate.getDate()}`,
  });

  const footerParagraph = new Paragraph({
    children: [
      pageNumberText,
      pagesText,
      dateText.tab(),
    ],
    tabStops: [{
      type: TabStopType.RIGHT,
      position: TabStopPosition.MAX,
    }],
  });

  const footer = new Footer({
    children: [footerParagraph],
  });

  const topBottomMarginDXA = convertInchesToDXA(0.5);
  const leftRightMarginDXA = convertInchesToDXA(0.75);

  doc.addSection({
    footers: {
      default: footer,
    },
    children: [
      new Table({
        rows: tableRows,
        width: {
          type: WidthType.DXA,
          size: convertInchesToDXA(6.75),
        },
        columnWidths: [convertInchesToDXA(2.6), convertInchesToDXA(imageCellWidth)],
      }),
    ],
    margins: {
      top: topBottomMarginDXA,
      bottom: topBottomMarginDXA,
      left: leftRightMarginDXA,
      right: leftRightMarginDXA,
    },
  });

  Packer.toBlob(doc).then((blob) => {
    saveAs(blob, saveName);
  });
};

export default buildDocument;
