import { convertInchesToDXA, imageCellHeight, imageCellWidth } from '../../buildDocument';

const addImageProcess = (src: string): Promise<{height: number, width: number}> => (
  new Promise((resolve, reject) => {
    const imgForDim = new Image();
    imgForDim.onload = () => {
      resolve({ height: imgForDim.height, width: imgForDim.width });
    };
    imgForDim.onerror = reject;
    imgForDim.src = src;
  }));

export const getOutputDimensions = async (image: string) => {
  let outputDimensions;
  try {
    outputDimensions = await addImageProcess(image);
  } catch (e) {
    throw new Error(e.message);
  }
  const maxWidth = convertInchesToDXA(imageCellWidth / 16);
  const maxHeight = convertInchesToDXA(imageCellHeight / 12);

  const pratio = outputDimensions.width / outputDimensions.height;
  if (pratio > 1) {
    if (outputDimensions.width > maxWidth) {
      outputDimensions.width = maxWidth;
      outputDimensions.height = maxWidth / pratio;
    } else {
      outputDimensions.height = maxHeight;
      outputDimensions.width = maxHeight * pratio;
    }
  } else {
    outputDimensions.height = maxHeight;
    outputDimensions.width = maxHeight * pratio;
  }
  return outputDimensions;
};

export default getOutputDimensions;
