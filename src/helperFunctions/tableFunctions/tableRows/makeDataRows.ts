import {
  AlignmentType,
  File,
  HeightRule,
  ITextRunOptions,
  Media,
  Paragraph,
  TableCell,
  TableRow,
  TextRun,
  VerticalAlign,
} from 'docx';
import { borderStyle, convertInchesToDXA, imageCellHeight } from '../../buildDocument';
import { RowData } from '../../../App';
import { getOutputDimensions } from './processImage';

const makeDataRow = async (doc: File, data: RowData, photoNumber: number): Promise<TableRow[]> => {
  const textOptions: ITextRunOptions = {
    text: 'Text',
    font: { name: 'Times New Roman' },
    size: 24,
    bold: true,
  };

  const img = await fetch(data.preview);
  const imageBuffer = await img.arrayBuffer();
  const outputDimensions = await getOutputDimensions(data.preview);
  const rowImage = Media.addImage(
    doc,
    imageBuffer,
    outputDimensions.width,
    outputDimensions.height,
  );

  const photographContent = new Paragraph({
    children: [rowImage],
    alignment: AlignmentType.CENTER,
  });

  const newInfoParagraph = (text: string) => (
    new Paragraph({
      children: [
        new TextRun({
          ...textOptions,
          text,
        }),
      ],
      alignment: AlignmentType.LEFT,
      indent: {
        left: convertInchesToDXA(0.05),
      },
    })
  );

  const newInfoTableCell = (
    text: string,
    verticalAlign = VerticalAlign.CENTER,
  ) => (
    new TableCell({
      children: [newInfoParagraph(text)],
      verticalAlign,
      borders: {
        left: borderStyle,
      },
      margins: {
        top: convertInchesToDXA(0.05),
      },
    })
  );

  return [
    new TableRow({
      children: [
        newInfoTableCell(`Photograph ID: ${photoNumber}`),
        new TableCell({
          children: [photographContent],
          rowSpan: 4,
          columnSpan: 1,
          verticalAlign: VerticalAlign.CENTER,
        }),
      ],
      height: {
        height: convertInchesToDXA(0.35),
        rule: HeightRule.EXACT,
      },
    }),
    new TableRow({
      children: [newInfoTableCell(`Date: ${data.date.toLocaleDateString()}`)],
      height: {
        height: convertInchesToDXA(0.35),
        rule: HeightRule.EXACT,
      },
    }),
    new TableRow({
      children: [newInfoTableCell(`Direction: ${data.direction}`)],
      height: {
        height: convertInchesToDXA(0.35),
        rule: HeightRule.EXACT,
      },
    }),
    new TableRow({
      children: [newInfoTableCell(`Comments: ${data.comments}`, VerticalAlign.TOP)],
      height: {
        height: convertInchesToDXA(imageCellHeight),
        rule: HeightRule.EXACT,
      },
    }),
  ];
};

export default makeDataRow;
