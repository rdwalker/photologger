import {
  AlignmentType, HeightRule, Paragraph, TableCell, TableRow, TextRun, VerticalAlign,
} from 'docx';

import { convertInchesToDXA, borderStyle } from '../../buildDocument';

const makeTitleRow = () => {
  const rowText = new TextRun({
    text: 'Photographic Record',
    bold: true,
    size: 26,
    font: { name: 'Times New Roman' },
  });

  const rowContent = new Paragraph({
    children: [rowText],
    alignment: AlignmentType.CENTER,
  });

  return new TableRow({
    children: [
      new TableCell({
        children: [
          rowContent,
        ],
        columnSpan: 2,
        verticalAlign: VerticalAlign.CENTER,
        borders: {
          top: borderStyle,
          left: borderStyle,
          right: borderStyle,
        },
      }),
    ],
    tableHeader: true,
    height: {
      height: convertInchesToDXA(0.5),
      rule: HeightRule.EXACT,
    },
  });
};

export default makeTitleRow;
