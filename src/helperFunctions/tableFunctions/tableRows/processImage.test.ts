import getOutputDimensions from './processImage';

describe('processImage', () => {
  const LOAD_FAILURE_SRC = 'LOAD_FAILURE_SRC';
  const LOAD_SUCCESS_SRC = 'LOAD_SUCCESS_SRC';

  beforeAll(() => {
    Object.defineProperties(Image.prototype, {
      width: {
        value: 100,
      },
      height: {
        value: 50,
      },
      src: {
        set(src) {
          if (src === LOAD_FAILURE_SRC) {
            setTimeout(() => this.onerror(new Error('mocked error')));
          } else if (src === LOAD_SUCCESS_SRC) {
            setTimeout(() => this.onload());
          }
        },
      },
    });
  });

  it('should return the dimensions of the image if pratio > 1 and width < maxWidth', async () => {
    expect(await getOutputDimensions(LOAD_SUCCESS_SRC)).toEqual({ height: 404.4, width: 808.8 });
  });

  it('should return the dimensions of the image if pratio > 1 and width > maxWidth', async () => {
    Object.defineProperties(Image.prototype, {
      width: {
        value: 1000,
      },
      height: {
        value: 700,
      },
    });
    expect(await getOutputDimensions(LOAD_SUCCESS_SRC)).toEqual({ height: 261.45, width: 373.5 });
  });

  it('should return the dimensions of the image if pratio <= 1', async () => {
    Object.defineProperties(Image.prototype, {
      width: {
        value: 350,
      },
      height: {
        value: 700,
      },
    });
    expect(await getOutputDimensions(LOAD_SUCCESS_SRC)).toEqual({ height: 404.4, width: 202.2 });
  });

  it('should reject with error', async () => {
    try {
      await getOutputDimensions(LOAD_FAILURE_SRC);
    } catch (e) {
      expect(e.message).toMatch('mocked error');
    }
  });
});
