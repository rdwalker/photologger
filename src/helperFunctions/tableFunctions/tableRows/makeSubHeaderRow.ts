import {
  AlignmentType, HeightRule,
  ITextRunOptions,
  Paragraph,
  TableCell,
  TableRow,
  TextRun,
  VerticalAlign,
} from 'docx';

import { convertInchesToDXA, borderStyle } from '../../buildDocument';

const makeSubHeaderRow = (
  column1Prompt: string,
  column1Value: string,
  column2Prompt: string,
  column2Value: string,
) => {
  const textOptions: ITextRunOptions = {
    text: 'Text',
    font: { name: 'Times New Roman' },
    size: 24,
    bold: true,
  };

  const column1Text = new TextRun({
    ...textOptions,
    text: `${column1Prompt}: ${column1Value}`,
  });

  const column2Text = new TextRun({
    ...textOptions,
    text: `${column2Prompt}: ${column2Value}`,
  });

  const columnProperties = {
    alignment: AlignmentType.LEFT,
    indent: {
      left: convertInchesToDXA(0.05),
    },
  };

  const column1Content = new Paragraph({
    ...columnProperties,
    children: [column1Text],
  });

  const column2Content = new Paragraph({
    ...columnProperties,
    children: [column2Text],
  });

  const cellProperties = {
    verticalAlign: VerticalAlign.CENTER,
    borders: {
      left: borderStyle,
    },
    margins: {
      top: convertInchesToDXA(0.1),
      bottom: convertInchesToDXA(0.1),
    },
  };

  const subheaderCell1 = new TableCell({
    ...cellProperties,
    children: [
      column1Content,
    ],
  });

  const subheaderCell2 = new TableCell({
    ...cellProperties,
    children: [
      column2Content,
    ],
  });

  return new TableRow({
    children: [
      subheaderCell1,
      subheaderCell2,
    ],
    tableHeader: true,
    height: {
      height: convertInchesToDXA(0.4),
      rule: HeightRule.EXACT,
    },
  });
};

export default makeSubHeaderRow;
