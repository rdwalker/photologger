import React from 'react';

import { Column } from 'material-table';
import { RowData } from 'App';

const columns: Column<RowData>[] = [
  {
    title: 'Name',
    field: 'imgName',
    editable: 'never',
    hidden: true,
  },
  {
    title: 'Preview',
    field: 'preview',
    render: (rowData: RowData) => (
      <img src={rowData.preview} style={{ width: 50 }} alt="Upload Preview" />
    ),
    editable: 'never',
  },
  {
    title: 'Date',
    field: 'date',
    type: 'datetime',
  },
  {
    title: 'Direction',
    field: 'direction',
    lookup: {
      N: 'N', NE: 'NE', E: 'E', SE: 'SE', S: 'S', SW: 'SW', W: 'W', NW: 'NW',
    },
  },
  {
    title: 'Comments',
    field: 'comments',
  },
];

export default columns;
